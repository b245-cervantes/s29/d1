db.users.insertOne({
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345678",
        email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations"
    })

//[SECTION] comparison Query Operators

    //$gt/$gte operator

       /*

          -allow us to fine documents that have field values greater than or
          equal to a specified value.
          - syntax:
             db.collectionName.find({field: {$gt:value}})
             db.collectionName.find({field: {$gte:value}})

       */

    db.users.find({
        age:{
            $gt:65 // this will return only 2
        }
    })

      db.users.find({
        age:{
            $gte:65 // will return 3 records including the age equal to 65
        }
    })

      //$le/$lte operator

       /*

          -allow us to fine documents that have field values less than or
          equal to a specified value.
          - syntax:
             db.collectionName.find({field: {$lt:value}})
             db.collectionName.find({field: {$lte:value}})

       */

      db.users.find({
        age:{
            $lt:65
        }
    });

       db.users.find({
        age:{
            $lte:65
        }
    });

       // $ne operator

          /*

             -allow us to find documents that have field number values not equal
             to specified value..
             - sysntax:
                 db.collectionName.find({
                   field: {$ne:{value}}
                 })


          */

       db.users.find({age: {$ne:82}})

       // $in operator
       /*

            --allow us to find documents with specific match criteria of one field using different values.
            -Syntax:
            db:collectionName.find({field:{$in:{[valueA, valueB]}}})


       */

       db.users.find({lastName: {$in:["Hawking", "Doe"]}})
       db.users.find({courses: {$in:["HTML", "React"]}})

       //[SECTION] 
         //$or operator
          /*
              -Allow us to find documents that match a single criteria
              from multiple provided search criteria

              syntax:
              db.collectionName.find({$or: [{fieldA:valueA}, {fieldB:valueB}]})


          */


       db.users.find({
        $or:[
                 {firstName:"Neil"},
                 {age: 25}
 
            ]
       })

        //With comparison query operator
       db.users.find({
        $or:[
                 {firstName:"Neil"},
                 {age: {$gt:30}}
 
            ]
       })

       //$and

       /*
              -Allow us to find documents matching multiple criteria
              in a single field.
              -syntax:

              db.collectionName.find({$and: [{fieldA:valueA}, {fieldB:valueB}]})


       */


       db.users.find({
        $and: [
              
              {age:{$ne:82}},
              {age:{$ne:76}}

            ]
       })

       db.users.find({
        $and: [

              {courses:{$in["Laravel","React"]}},
              {age: {$lt:80}}
            ]
       })

    //[SECTION] Field Projection
          //to help with the readability of the values returned, we can includ/exclude fields
       // from retrieve results.

          //Inclusion
         /*
             -Allow us to include/add specific fields only
             when retrieving documents.
             -The value provided is 1 to denote that the field
             being included.
             -syntax:

                 db.users.find({criteria}, {field: 1})


         */

       db.users.find({
        firstName: "Jane"
       },
       {
        firstName:1,
        lastName:1,
        contact: 1,
       })

       //exclusion

       /*

            -allow us to exclude/remove specific fields only when
             retrieving documents.
             --the value provided is 0 to denote that the field
             is being excluded
             -syntax:

                 db.users.find({criteria}, {field: 0})
       */

       db.users.find({
        firstName:"Jane"
       },
       {
        _id:0,
        contact:0,
        department:0
       }
       )


       db.users.find({
        lastName: "Doe"
       },
       {
        firstName: 1,
        lastName:1,
        contact:1
        _id:0
       })

       //Supressing the ID field
          // - when using field projection, field
          //inclusion and exclusion may not used at the same time
          // - excluding the "_id" field is the only exception to this rule.
       // Syntax: db.collectionName.find({Criteria}, {
        // {field:1, id: 0}
       //})

       // Return a specifi field in Embedded documents.
       db.user.find(
       {
            firstName; "Jane"
       },
       {
            firstName: 1,
            lastName:1,
            "contact.phone": 1
       }
       )

       //Exclude

       db.user.find(
        {
            firstName: "Jane"
        },
        {
            "contact.phone": 0
        }

        );
       //Project specific elements in the returned array.
        //The $slice operator allows us to retrieve
       //element that matches the criteria.
       //Syntax:
       //   db.collectionName.find({criteria}, arrayfield: {$slice:count});
       //   db.collectionName.find({criteria}, arrayfield: {$slice:[index, count]});
        //show the elements array based on slice count.
       db.users.find(
         {
            firstName: "Jane"
         },
         {
            courses: {$slice:1}
         }
        )

       db.users.find(
         {
            firstName: "Jane"
         },
         {
            courses: {$slice:[1,1]}
         }
        )

       //[SECTION] Evaluation Query Operator

           db.users.find({firstName: "jane"})

           //$regex operator

              /*
                  -Allows us to find documents that match a specific string pattern
                  using regular expression/ "regex"

                  syntax:

                  db.collectionName.find({field: $regex: "pattern", $option: "optionValue"})


              */

           //case sensitive query
           db.users.find({firstName:{$regex:"ne"}})

           //case insensitive query
           db.users.find({firstName:{$regex:"ne",$options: "$i"}})
